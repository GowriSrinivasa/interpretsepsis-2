from models import RandomForest, Linear_SVC, AdaBoost, GradientBoost
import pandas as pd
from train import get_cv_scores, get_scores
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import RandomOverSampler
from sklearn.ensemble import VotingClassifier
from itertools import combinations

def ensemble(X_train, X_test, y_train, y_test):
    ada_boost_model = AdaBoost(X_train, y_train)            
    grad_boost_model = GradientBoost(X_train, y_train)
    linear_svc_model = Linear_SVC(X_train, y_train)
    random_forest_model = RandomForest(X_train, y_train)

    model_list = [('AdaBoost', ada_boost_model), ('GradBoost', grad_boost_model), ('SVC', linear_svc_model), ('RF', random_forest_model)]

    # 2 at a time:
    for choice in combinations(model_list, 2):
        print("MODELS:", list(choice))

        if("SVC" in [item[0] for item in list(choice)]):
            voting_clf = VotingClassifier(estimators=list(choice), voting='hard')
        else:
            voting_clf = VotingClassifier(estimators=list(choice), voting='soft')

        voting_clf.fit(X_train, y_train)
        get_scores(voting_clf, X_train, X_test, y_train, y_test)
    

    for choice in combinations(model_list, 3):
        print("MODELS:", list(choice))

        if("SVC" in [item[0] for item in list(choice)]):
            voting_clf = VotingClassifier(estimators=list(choice), voting='hard')
        else:
            voting_clf = VotingClassifier(estimators=list(choice), voting='soft')

        voting_clf.fit(X_train, y_train)
        get_scores(voting_clf, X_train, X_test, y_train, y_test)
    
    # all 4 together:
    print("MODELS:", model_list)
    voting_clf = VotingClassifier(estimators=model_list, voting='hard')
    voting_clf.fit(X_train, y_train)
    get_scores(voting_clf, X_train, X_test, y_train, y_test)